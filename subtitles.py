#!/usr/bin/env python3

import sys
from os import listdir, path
from urllib import request
import zipfile
import io
import re


def get_largest_file():
    f_names = [f_name for f_name in listdir() if path.isfile(f_name)]
    sorted_f_names = sorted(f_names, key=path.getsize, reverse=True)
    largest_f_name = sorted_f_names[0]
    return largest_f_name


def remove_extension(f_name):
    f_basename, _ = path.splitext(f_name)
    return f_basename


def fetch_subtitles(url):
    with request.urlopen(url) as response:
        f_name = response.info().get_filename()
        sub_bytes = response.read()
        if f_name.endswith(".zip"):
            with zipfile.ZipFile(io.BytesIO(sub_bytes)) as sub_zip:
                subtitles = list(
                    map(
                        lambda member_name: {
                            "content": sub_zip.read(member_name).decode("Windows-1250"),
                            "ext": path.splitext(member_name)[1],
                        },
                        sub_zip.namelist(),
                    )
                )
                return subtitles
        else:
            return [
                {
                    "content": sub_bytes.decode("Windows-1250"),
                    "ext": path.splitext(f_name)[1],
                }
            ]


def create_subtitle_file(sub_f_name, content):
    with open(sub_f_name, "w+") as sub_f:
        sub_f.write(content)


def create_subtitle_url(sub_id):
    return f"https://titlovi.com/download/?type=1&mediaid={sub_id}"


def extract_sub_id(link):
    direct_link_match = re.search(
        r"https:\/\/titlovi\.com\/download\/\?type=1&mediaid=(\d+)", link
    )
    if direct_link_match is not None:
        return direct_link_match.group(1)
    else:
        from_list_link_match = re.search(
            r"https:\/\/titlovi\.com\/titlovi\/.+-(\d+)", link
        )
        if from_list_link_match is not None:
            return from_list_link_match.group(1)
        else:
            raise ValueError("The link is invalid")


def download_subtitle(link):
    video_f_name = get_largest_file()
    video_f_basename = remove_extension(video_f_name)
    sub_id = extract_sub_id(link)
    sub_url = create_subtitle_url(sub_id)
    subtitles = fetch_subtitles(sub_url)
    for index, subtitle in enumerate(subtitles):
        sub_content = subtitle["content"]
        sub_ext = subtitle["ext"]
        create_subtitle_file(f"{video_f_basename}_ds{index}{sub_ext}", sub_content)


def main(argv):
    download_subtitle(argv[0])


if __name__ == "__main__":
    main(sys.argv[1:])
